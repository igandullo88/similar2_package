#' @title Detecta plagio en comentarios
#' @description Detecta plagio en comentarios de los scripts R or Rmd d eun directorio.
#' @param path directorio
#' @param k tamano del k-gram a aplicar en los algoritmos fingerprints.
#' @param t tamano minimo de palabra para la detección en el algoritmo winnowing.
#' @param type_hash algoritmo a utilizar para obtener las fingerprints ("winnow", "finger")
#' @param th_fps Valor umbral para la decision del algoritmo fingerprint/winnowing.
#' @param filter_comment lineas de comentarios del script que no se
#' quieren depurar. Valores valido TRUE, FALSE o data.frame. En caso de
#' TRUE, se pedira que introduzca cada linea.
#' @param dir_Rmd direnctorio secundario para guardar la conversion de archivos Rmd a R,
#' y directorio donde se crea la carpeta binder.
#' @param language idioma de los comentarios.
#' @import dplyr
#' @import magrittr
#' @export plagiarism_comments
plagiarism_comments = function(path, type_file = "R", dir_Rmd, k=4, t=7, th_fps = 0.7,
                               filter_comment=FALSE, language="spanish", encoding = "UTF-8"){
  names_dir = dir(path)
  if(type_file == "R"){
    names_dir = dir(path)[stringr::str_detect(names_dir, ".R")]
  }else{
    if(type_file == "Rmd"){
      names_dir = dir(path)[stringr::str_detect(names_dir, ".Rmd")]
    }else{
      stop("non-type file")
    }
  }
  dataset <- paste(path, names_dir, sep ="/")

  df_comentaries = list()
  h_fp_wn = list()
  freq_letter = list()

  for(i in 1:length(dataset)){
    df_comentaries[[i]] = df_comments(dataset[i],type_file = type_file, dir_Rmd = dir_Rmd,
                                      filter_comment = filter_comment, language = language,
                                      encoding = encoding)

    h_fp_wn[[i]] = Algorithm_fp_win(df_comentaries[[i]][[1]], k = k,t = t)

    freq_letter[[i]] = df_comentaries[[i]][[2]]
  }

  names(h_fp_wn) = names_dir
  names(freq_letter) = names_dir

  summaryPhi = summary_Phi(list_fps = h_fp_wn, names_file = names_dir, th_fps = th_fps)

  return(list(Matrix_Phi_fp = summaryPhi$Matriz_finger,
         Matrix_Phi_win = summaryPhi$Matriz_win,
         tab_res_Sorensen = summaryPhi$tab_Sorensen,
         freq_letter = freq_letter))
}




