#' @title Detecta plagio entre los archivos R o Rmd de un directorio.
#' @description Proporcionando un directorio de archivos R o Rmd cuantifica como de parecidos son los pares
#' de archivos segun el codigo, los comentarios, y calcula una proporcion se ambos segun el parametro p.
#' @param path archivo R o Rmd.
#' @param type_file tipo de archivo, valido "R" o "Rmd".
#' @param k tamano del k-gram a aplicar en los algoritmos fingerprints.
#' @param t tamano minimo de palabra para la detección en el algoritmo winnowing.
#' @param type_hash algoritmo a utilizar para obtener las fingerprints ("winnow", "finger")
#' @param p peso proporcionado al coeficiente Sorensen, entre 0-1
#' @param th_kap Valor umbral para la decision del algoritmo SimilaR.
#' @param th_fps Valor umbral para la decision del algoritmo fingerprint/winnowing.
#' @param th_Sim2 valor umbral para la decision generalizada proporcionada por SimilaR2
#' @param filter_comment lineas de comentarios del script que no se
#' quieren depurar. Valores valido TRUE, FALSE o data.frame. En caso de
#'  TRUE, se pedira que introduzca cada linea.
#' @param dir_Rmd direnctorio secundario para guardar la conversion de archivos Rmd a R,
#' y directorio donde se crea la carpeta binder.
#' @param cluster TRUE muestra análisis clustering.
#' @param breakdown_comments TRUE muestra resultados desagregados obtenidos de la detección de
#' plagio en los comentarios.
#' @param breakdown_code TRUE muestra resultados desagregados obtenidos de la detección de
#' plagio en el código.
#' @param View_wr_scp TRUE no elimina la carpeta binder. El usuario puede visualizar los scripts
#' reescritos por wfS.
#' @param cut_Kap tamano de cluster para la Matriz Kappa.
#' @param cut_Phi tamano de cluster para la Matriz Phi
#' @param agglomerative tipo de cluster jerarquico. TRUE aglomerativo, FALSE divisible.
#' @param language idioma de los comentarios.
#' @examples
#' SimilaR2("test")
#' SimilaR2("test", k = 4, t = 7)
#' @import cluster
#' @import dplyr
#' @import factoextra
#' @import ggplot2
#' @import purrr
#' @import SimilaR
#' @import stringi
#' @import stringr
#' @import knitr
#' @import tm
#' @import tibble
#' @import tidyr
#' @export SimilaR2
SimilaR2 = function(path,
                                type_file = "R",
                                k=4, t = 7,
                                type_hash = "winnow",
                                p = 0.5,
                                th_kap = 0.5,
                                th_fps = 0.5,
                                th_Sim2 = 0.5,
                                filter_comment = F,
                                cut_Kap = 3,
                                cut_Phi = 3,
                                cluster = FALSE,
                                agglomerative = TRUE,
                                breakdown_comments= FALSE,
                                breakdown_code = TRUE,
                                View_wr_scp = FALSE,
                                dir_Rmd = getwd(),
                                language= "spanish",
                                encoding = "UTF-8"){
  if(p>1 || p<0) stop("p in (0-1)")
  q = 1-p
  names_dir = dir(path)
  if(type_file == "R"){ names_dir = dir(path)[stringr::str_detect(names_dir, ".R")]
  }else{if(type_file == "Rmd"){names_dir = dir(path)[stringr::str_detect(names_dir, ".Rmd")]
  }else{ stop("non-type file")}}

  if(cut_Kap >length(names_dir) || cut_Phi >length(names_dir)) stop("Cluster size cannot be larger than the number of files to be compared")

  if(sum((type_hash != "winnow") || (type_hash != "finger"))== 0) stop("type_hash must be winnow or finge")
  ## plagiarism comments

  result_comments =
  plagiarism_comments(path = path, type_file = type_file, k = k, t = t, dir_Rmd = dir_Rmd,
                      th_fps = th_fps, filter_comment = filter_comment, language = language,
                      encoding = encoding)

  ## plagiarism code

  result_code =
  plagiarism_code(path = path, th_kap = th_kap, type_file = type_file,
                  dir_Rmd = dir_Rmd, View_wr_scp = View_wr_scp, encoding =  encoding)

  # Resultados finales

  table_Sorensen = result_comments$tab_res_Sorensen %>%
                   tidyr::unite(Files,c(1:2),  sep = " vs ", remove = TRUE)
  table_Kappa = result_code$table_Kappa %>%
                tidyr::unite(Files,c(1:2),  sep = " vs ", remove = TRUE)

  select_column = if(type_hash == "winnow"){c(1:3, pi_1 = 4, varphi = 7, pi_2= 8)
                  }else{c(1:3, pi_1 = 4, varphi = 5, pi_2= 6)}
  table_final = dplyr::inner_join( table_Kappa, table_Sorensen, by = "Files") %>%
                tidyr::separate(col = 1, into = c("file1", "file2"), sep = " vs ")%>%
                dplyr::select(select_column) %>%
                dplyr::mutate(theta = kappa * q + varphi *p,
                              Pi = ifelse(theta > th_Sim2,1,0)) %>%
                dplyr::arrange(-theta)


  # Matrices simétricas de distancia
   Matrix_Kappa =  result_code$Matriz_Kappa
   Matrix_Phi = if(type_hash == "winnow"){result_comments$Matrix_Phi_win
                }else{result_comments$Matrix_Phi_fp}

  ## Clustering
  if(cluster == TRUE){
    cluster_Kappa = clustering(Matrix_Kappa, cut_k = cut_Kap, agglomerative = agglomerative)
    cluster_Phi = clustering(Matrix_Phi, cut_k = cut_Phi, agglomerative = agglomerative)
  }

  ## Salidas

  final_ouptut = list(final_score = table_final, breakdown = list())
  i=1
  if(breakdown_code  == TRUE){
    final_ouptut[[2]][[i]] = list(SimilaR_mod = result_code$table_SimilaR_mod,
                                       table_Kappa = result_code$table_Kappa)
    names(final_ouptut$breakdown) = "breakdown_code"
    i=i+1
  }

  if(breakdown_comments==TRUE){
    final_ouptut[[2]][[i]] = list(table_Sorensen = result_comments$tab_res_Sorensen,
                                  freq_letter= result_comments$freq_letter)
    names(final_ouptut$breakdown)[i] = "breakdown_comments"
    i=i+1
  }

  if(cluster == TRUE){
    final_ouptut[[2]][[i]] = list(cluster_kappa = cluster_Kappa,
                                  cluster_Phi = cluster_Phi)
    names(final_ouptut$breakdown)[i] = "clustering"
  }
  return(final_ouptut)
}

