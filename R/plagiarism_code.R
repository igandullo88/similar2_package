#' @title Detecting plagiarism in codes
#' @description Detect plagiarism in code of scripts R or Rmd.
#' @param path archivo R o Rmd.
#' @param type_file tipo de archivo, valirdo "R" o "Rmd".
#' @param th_kap Valor umbral para la decision del algoritmo SimilaR.
#' @param View_wr_scp TRUE no elimina la carpeta binder. El usuario puede visualizar los scripts
#' reescritos por wfS.
#' @param dir_Rmd direnctorio secundario para guardar la conversion de archivos Rmd a R,
#' y directorio donde se crea la carpeta binder.
#' @export plagiarism_code
plagiarism_code = function(path, th_kap = 0.7, type_file = "R",  View_wr_scp = TRUE,
                            encoding= "UTF-8", dir_Rmd = dir_Rmd){

  names_dir = dir(path)
  if(type_file == "R"){
    names_dir = dir(path)[stringr::str_detect(names_dir, ".R")]
  }else{
    if(type_file == "Rmd"){
      names_dir = dir(path)[stringr::str_detect(names_dir, ".Rmd")]
    }else{
      stop("non-type file")
    }
  }
  dataset <- paste(path, names_dir, sep ="/")

  for(i in 1:length(dataset))
   write_func_SimilaR(file = dataset[i], type_file = type_file,
                      encoding = encoding, dir_Rmd = dir_Rmd)


  tab_SimilaR = SimilaR::SimilaR_fromDirectory(paste0(dir_Rmd,"/binder"),
                                 fileTypes = "file", aggregation = "both")%>%
                tidyr::separate(name1, sep = "   " , into = c("file1", "function1"))%>%
                tidyr::separate(name2, sep = "   " , into = c("file2", "function2"))

  if(View_wr_scp == FALSE || View_wr_scp == "FALSE") unlink("binder", recursive = TRUE)

  res_Kappa = summary_Kappa(tab_SimilaR = tab_SimilaR, names_dir = names_dir, th_kap = th_kap)
  return(list(table_SimilaR_mod = res_Kappa$table_SimilaR_mod,
              Matriz_Kappa =  res_Kappa$Matrix_Kappa,
              table_Kappa = res_Kappa$table_Kappa))
}
